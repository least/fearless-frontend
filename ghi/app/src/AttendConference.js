import { useState, useEffect } from 'react'
export default function AttendConference() {
  const [conferences, setConferences] = useState([])
  const [formData, setFormData] = useState([])
  const [spinnerClasses, setSpinnerClasses] = useState('d-flex justify-content-center mb-3')
  const [dropdownClasses, setDropdownClasses] = useState('d-flex justify-content-center mb-3')
  const [success, setSuccess] = useState(false)


  useEffect(() => {

    const fetchConferences = async () => {

      try {

        const url = 'http://localhost:8000/api/conferences/';
        const res = await fetch(url)
        const data = await res.json()
        setConferences(data.conferences)
      }
      catch (e) {
        console.error(e)
      }
    }

    fetchConferences()

  }, [])

  useEffect(() => {
    // console.log(conferences)
    if (conferences) {
      setSpinnerClasses('d-flex justify-content-center mb-3 d-none')
      setDropdownClasses('form-select')
    }
  }, [conferences])

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  useEffect(() => {

    console.log(formData)
  }, [formData])

  const handleSubmit = async e => {
    e.preventDefault()
    // console.log('preventing default')
    const attendeesUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await fetch(attendeesUrl, fetchConfig)
    if (res.ok) {
      const newAttendee = await res.json()
      console.log(newAttendee)
      setSuccess(true)
    }
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              {success ? <div className="alert alert-success mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
                :
                <form id="create-attendee-form" onSubmit={handleSubmit}>
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                  </p>
                  <div className={spinnerClasses} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>

                  <div className="mb-3">
                    <select name="conference" id="conference" className={dropdownClasses} required onChange={handleChange}>
                      <option value="">Choose a conference</option>
                      {conferences.map(conference => {
                        return (<option key={conference.name + conference.name} value={conference.href}>{conference.name}</option>)
                      })}
                    </select>
                  </div>
                  <p className="mb-3">
                    Now, tell us about yourself.
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input required placeholder="Your full name" type="text" id="name" name="name"
                          className="form-control" onChange={handleChange} />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input required placeholder="Your email address" type="email" id="email" name="email"
                          className="form-control" onChange={handleChange} />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>}

            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

