import { useState, useEffect, useRef } from "react"
export default function NewLocation() {
  const [formData, setFormData] = useState([])
  const [states, setStates] = useState([])
  const formRef = useRef(null)

  // fetch states on load
  useEffect(() => {

    const fetchStates = async () => {

      try {
        const url = 'http://localhost:8000/api/states/'
        const res = await fetch(url)
        const data = await res.json()
        setStates(data.states)

      }
      catch (e) {
        console.error("Could not fetch states, ", e)
      }
    }

    fetchStates()

  }, [])

  useEffect(() => {

    // console.log(formData)
  }, [formData])


  const handleSubmit = async e => {
    e.preventDefault()
    // console.log('preventing deonfault')

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };


    try {

      const res = await fetch(locationUrl, fetchConfig)
      if (res.ok) {

        const newLocation = await res.json()
        console.log(newLocation)
        // TODO: success message
        formRef.current.reset()
      }
      else {
        // TODO: fail message
      }
    }
    catch (e) {
      console.error("Could not add location,", e)
    }

  }

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
    // console.log(formData)
  }
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form ref={formRef} id="create-location-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input placeholder="Name" required type="text" id="name" name="name" className="form-control" onChange={handleChange} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Room count" required type="number" name="room_count" id="room_count"
                className="form-control" onChange={handleChange} />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="City" required type="text" name="city" id="city" className="form-control" onChange={handleChange} />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select required name="state" id="state" className="form-select" onChange={handleChange}>
                <option defaultValue value="">Choose a state</option>
                {
                  states.map((state, index) => {
                    return (<option key={state.name + index} value={state.abbreviation}>{state.name}</option>)
                  })
                }
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
