import { useState, useEffect, useRef } from 'react'
export default function NewPresentation() {
  const [conferences, setConferences] = useState([])
  const [formData, setFormData] = useState([])
  const [conferenceId, setConferenceId] = useState([])
  const formRef = useRef(null)


  // fetch all conferences once on load
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('http://localhost:8000/api/conferences/')
        const data = await res.json()
        setConferences(data.conferences)
      }
      catch (e) {
        console.error(e)
      }
    }

    fetchData()
  }, [])

  useEffect(() => {
    // console.log(formData)
    // console.log(conferenceId)
  }, [formData, conferenceId])

  const handleSubmit = async e => {
    e.preventDefault()
    // console.log('preventing default')
    // console.log(formData)

    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const res = await fetch(presentationUrl, fetchConfig)
      // console.log(res)
      if (res.ok) {
        const newPresentation = await res.json()
        formRef.current.reset()
        // console.log(newPresentation)
        // TODO: show success status on page
      }
      else {

        // TODO: show failure status on <page class=":w"></page>
      }
    }
    catch (e) {
      console.error(e)
    }

  }

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const changeConferenceId = (e) => {
    setConferenceId(e.target.value)
    // console.log('changed conference id')
  }


  return (<div className="row">
    <div className="offset-1 col-10">
      <div className="shadow p-4 mt-4">
        <h1>Create a new Presentation</h1>
        <form ref={formRef} id="create-presentation-form" onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input placeholder="Presenter Name" required type="text" id="presenter_name" name="presenter_name" className="form-control" onChange={handleChange} />
            <label htmlFor="name">Presenter Name</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Presenter Email" required type="email" id="presenter_email" name="presenter_email" className="form-control" onChange={handleChange} />
            <label htmlFor="name">Presenter Email</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Company Name" type="text" id="company_name" name="company_name" className="form-control" onChange={handleChange} />
            <label htmlFor="name">Company Name</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Title" required type="text" id="title" name="title" className="form-control" onChange={handleChange} /> <label htmlFor="name">Title</label>
          </div>
          <div className="form-floating mb-3">
            <textarea className="form-control" placeholder="synopsis" name="synopsis" rows="3" cols="" onChange={handleChange} ></textarea>
            <label htmlFor="synopsis">Synopsis</label>
          </div>
          <div className="mb-3">
            <select required name="conference" id="conference" className="form-select" onChange={changeConferenceId}>
              <option defaultValue value="">Choose a Conference</option>
              {conferences.map(conference => {
                return (<option value={conference.id} key={conference.name + conference.id}>{conference.name}</option>)
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  )
}
