import { useEffect, useState, useRef } from "react"
export default function ConferenceForm() {
  const [formData, setFormData] = useState([])
  const [locations, setLocations] = useState([])
  const formRef = useRef(null)

  // on load, fetch locations 
  useEffect(() => {
    const fetchLocations = async () => {
      const url = "http://localhost:8000/api/locations/"
      try {
        const res = await fetch(url)
        const data = await res.json()
        setLocations(data.locations)
        // console.log(locations)

      }
      catch (e) { console.error("Couldn't fetch locations", e) }
    }

    fetchLocations()
  }, [])

  const handleSubmit = async e => {
    e.preventDefault()
    try {
      const ConferenceUrl = "http://localhost:8000/api/conferences/"
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json'
        }
      }

      const res = await fetch(ConferenceUrl, fetchConfig)
      if (res.ok) {

        const newConference = await res.json()
        // console.log(newConference)
        formRef.current.reset()
        // TODO: success msg
      }
      else {
        // TODO: err msg
      }


    }
    catch (e) {
      console.error(e)
    }
  }

  // useEffect(() => {
  //   console.log(formData)
  // }, [formData])

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  return (
    <div className="row">
      <div className="offset-1 col-10">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form ref={formRef} id="create-conference-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input placeholder="Name" required type="text" id="name" name="name" className="form-control" onChange={handleChange} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="starts" required type="date" id="starts" name="starts" className="form-control" onChange={handleChange} />
              <label htmlFor="name">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="ends" required type="date" id="ends" name="ends" className="form-control" onChange={handleChange} />
              <label htmlFor="name">Ends</label>
            </div>

            <div className="form-floating mb-3">
              <input placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees"
                className="form-control" onChange={handleChange} />
              <label htmlFor="room_count">Max Attendees</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Max Presentations" required type="number" name="max_presentations"
                id="max_presentations" className="form-control" onChange={handleChange} />
              <label htmlFor="room_count">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <textarea className="form-control" placeholder="description" name="description" rows="3" cols="" onChange={handleChange}></textarea>
              <label htmlFor="description">Description</label>
            </div>
            <div className="mb-3">
              <select name="location" id="location" className="form-select" onChange={handleChange}>
                <option defaultValue value="">Choose a Location</option>
                {locations.map(location => {
                  return (<option key={"location " + location.id} value={location.id}>{location.name}</option>)
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>

  )
}

